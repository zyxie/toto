package com.example.demo.rest;

import com.example.demo.bean.Book;
import com.example.demo.bean.BookDto;
import com.example.demo.service.BookApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class BookController {
    @Autowired
    private BookApi bookApi;

    @PostMapping(path = "/add-book", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BookDto> addBook(@Valid @RequestBody Book book) {
        BookDto bookDto = bookApi.getBookDto(book);
        return ResponseEntity.ok(bookDto);
    }
}
