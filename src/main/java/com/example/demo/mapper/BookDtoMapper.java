package com.example.demo.mapper;

import com.example.demo.bean.Book;
import com.example.demo.bean.BookDto;
import org.springframework.stereotype.Service;

@Service
public class BookDtoMapper {

    public BookDto fillBookDto(Book book) {
        if (book == null) {
            return null;
        }
        BookDto bookDto = new BookDto();
        bookDto.setTitle(book.getTitle());
        bookDto.setAuthor(book.getAuthor());
        bookDto.setAdresse(book.getAdresse());
        return bookDto;
    }

}
