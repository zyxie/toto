package com.example.demo.bean;

import javax.validation.constraints.NotBlank;

public class Adresse {
    @NotBlank
    private String ville;

    private Integer numero;

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }
}
