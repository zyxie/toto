package com.example.demo.bean;

import java.util.UUID;

public class BookDto extends Book {
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
