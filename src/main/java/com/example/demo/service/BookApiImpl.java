package com.example.demo.service;

import com.example.demo.bean.Book;
import com.example.demo.bean.BookDto;
import com.example.demo.repository.BookRepositoryApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookApiImpl implements BookApi {
    @Autowired
    private BookRepositoryApi bookRepositoryApi;

    @Override
    public BookDto getBookDto(Book book) {
        return bookRepositoryApi.getBookDto(book);
    }
}
