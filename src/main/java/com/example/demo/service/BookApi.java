package com.example.demo.service;

import com.example.demo.bean.Book;
import com.example.demo.bean.BookDto;

public interface BookApi {
    BookDto getBookDto(Book book);
}
