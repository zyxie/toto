package com.example.demo.repository;

import com.example.demo.bean.Book;
import com.example.demo.bean.BookDto;
import com.example.demo.mapper.BookDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public class BookRepositoryApiImpl implements BookRepositoryApi {

    @Autowired
    private BookDtoMapper bookDtoMapper;

    @Override
    public BookDto getBookDto(Book book) {
        BookDto bookDto = bookDtoMapper.fillBookDto(book);
        bookDto.setId(UUID.randomUUID());
        return bookDto;
    }
}
