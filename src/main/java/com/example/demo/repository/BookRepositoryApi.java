package com.example.demo.repository;

import com.example.demo.bean.Book;
import com.example.demo.bean.BookDto;

public interface BookRepositoryApi {
    BookDto getBookDto(Book book);
}
